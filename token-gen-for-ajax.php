<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

header('Content-Type: application/x-www-form-urlencoded');

$headers = apache_request_headers();

if (isset($headers['Csrftoken'])) {
    if ($headers['Csrftoken'] !== $_SESSION['csrf_token']) {
        exit(json_encode(['error' => 'Wrong CSRF token.']));
    }
} else {
    exit(json_encode(['error' => 'No CSRF token.']));
}

$my_customer_id = '0a17251801ce1e32ca18fad236120de9';

$token = generate_token(24);
$client_ip = get_ip_address();        

$result = file_get_contents("http://intl.rest.sakkarinsai8.com/api.php/$my_customer_id/$client_ip/$token/no");
$result = json_decode($result, true);

if($result['result'] != 'successfully'){
    $token = "no_token";
}

echo $token;

function generate_token($length) {
    $characters = '123456789abcdefghijklmnpqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function get_ip_address(){
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
        if (array_key_exists($key, $_SERVER) === true){
            foreach (explode(',', $_SERVER[$key]) as $ip){
                $ip = trim($ip); // just to be safe
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                    return $ip;
                }
            }
        }
    }
}

?>