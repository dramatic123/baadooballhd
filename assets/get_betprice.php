<?php

session_start();

header('Content-Type: application/x-www-form-urlencoded');

$headers = apache_request_headers();
if (isset($headers['Csrftoken'])) {
    if ($headers['Csrftoken'] !== $_SESSION['csrf_token']) {
        exit(json_encode(['error' => 'Wrong CSRF token.']));
    }
} else {
    exit(json_encode(['error' => 'No CSRF token.']));
}
                   
    if (isset($_POST["betprice_id"]) && !empty($_POST["betprice_id"])) {
        $betprice_id = $_POST['betprice_id'];
        $betprice_info = file_get_contents("https://odds-api.sakkarinsai8.com/ufabet/$betprice_id");
        $betprice_info = json_decode($betprice_info);
        $data = array(
            'success' => true,
            'data' => $betprice_info->data
        ); 
    }else{
        $data = array(
            'success' => false            
        );
    }
    
    print json_encode($data);


?>