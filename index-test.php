<?php

$my_customer_id = '0a17251801ce1e32ca18fad236120de9';

function DateThai($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));		
		$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
	} 

?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>BaadooballHD.com ดูบอลสดครบทุกลีค</title>
    <link rel="icon" type="image/ico" href="img/title.ico" />
      <meta property="og:url"           content="http://www.BaadooballHD.com" />
      <meta property="og:type"          content="website" />
      <meta property="og:title"         content="BaadooballHD" />
      <meta property="og:description"   content="บ้าดูบอลHD ดูบอล ดูบอลฟรี ดูบอลพรีเมียร์ลีก ดูบอลสด ดูบอลออนไลน์  ข่าว ฟุตบอล"/>
      <meta name="keyword" content="baadooballhd.com,เว็บไซต์ดูบอลสด,ดูบอลออนไลน์,คมชัดระดับ HD,ไม่มีกระตุก,ดูบอลสดฟรี,ดูบอลสดมือถือ,
ผลบอล,โปรแกรมบอล,ลิ้งค์ดูบอล,ครบทุกลีก,พรีเมียร์ลีก,ไทยลีก,ลาลีกา,บุนเดสลีก้า,ลีกเอง,กัลโช่เซเรียอา,อัพเดทตลอด 24 ชม"/>
      <meta property="og:image"         content="http://www.BaadooballHD.com/img/header/3.jpg" />
    <!-- Bootstrap Core CSS -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="lib/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="lib/device-mockups/device-mockups.min.css">

    <!-- Theme CSS -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/card.css" rel="stylesheet">
    
</head>

<body id="bg">
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Gangbaaball</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php"><img class="img-responsive" src="img/logo/logo.png"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a class="page-scroll" href="#bg">ดูบอลสด</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#stream">ตารางบอล</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="live.php">ผลบอลสด</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="fb">
                        <a href="https://www.facebook.com/%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%94%E0%B8%B9%E0%B8%9A%E0%B8%AD%E0%B8%A5%E0%B8%84%E0%B8%AD%E0%B8%A1-701554886694882/"><i class="fa fa-facebook"></i></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
<!--  <div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
              <a href="https://line.me/R/ti/p/%40fxc7801t">
          <img class="img-responsive" src="img/advertice2.gif">
              </a>
        </div>
    </div>
</div> -->
    <header>
      <div class="container">
                <div id="main-slider" class="carousel slide" data-ride="carousel">
               <ol class="carousel-indicators">
                    <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                    <li data-target="#main-slider" data-slide-to="1"></li>
<!--                     <li data-target="#main-slider" data-slide-to="2"></li> -->
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                      <a href="https://line.me/R/ti/p/%40wys1883s">
                         <img class="img-responsive" width="100%" src="img/Slider3.jpg" alt="slider">  
                        </a> 
                    </div>
                    <div class="item">
                      <a href="https://line.me/R/ti/p/%40wys1883s">
                         <img class="img-responsive" width="100%" src="img/Slider4.jpg" alt="slider">  
                      </a> 
                    </div>
<!--                     <div class="item">
                         <img class="img-responsive" width="100%" src="img/header/slide3.jpg" alt="slider">  
                    </div>
 -->                
                    
                    <a class="left carousel-control" href="#main-slider" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left"></span>
                  </a> 
                    <a class="right carousel-control" href="#main-slider" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right"></span>
                  </a>           
               </div>
          </div>
          <a href="https://line.me/R/ti/p/%40pdq0688f"><img class="img-responsive" src="img/Slider33.gif"></a>
          <a href="https://line.me/R/ti/p/%40mzf7493p"><img class="img-responsive" src="img/Slider3333.gif"></a>
          <a href="https://line.me/R/ti/p/%40maba"><img class="img-responsive" width="100%" src="img/maba.gif"></a>
          </div>

<!--           <div id="banner"><a href="https://goo.gl/sPkuMX"><img class="img-responsive" src="img/b1.gif"></a></div>
          <div id="banner"><a href="https://www.facebook.com/groups/134042017277735/requests/?notif_id=1515658201498386&notif_t=group_r2j"><img class="img-responsive" src="img/789.gif"></a></div> -->

    </header>
<div class="side">
	<a href="https://line.me/R/ti/p/%40wys1883s">
		<div id="right">
			<img class="img-responsive" src="img/right.gif">
		</div>
	</a>
	<a href="https://line.me/R/ti/p/%40wys1883s">
		<div id="left">
			<img class="img-responsive" src="img/left.gif">
		</div>
	</a>
</div>

<section>
  <div class="container" >
    <div class="row">
      <div class="col-xs-12">


        <?php 
              
              date_default_timezone_set('Asia/Bangkok');
              $yesterday = date('Y-m-d',strtotime("-1 days"));
              $today = new DateTime('today');
              $today = $today->format('Y-m-d');
              $tomorrow = new DateTime('tomorrow');
              $tomorrow = $tomorrow->format('Y-m-d');
            
              $date = array($yesterday, $today, $tomorrow);
  
                
              $schedule = file_get_contents("http://www.sakkarinsai8.com/api-schedule/$my_customer_id/th");
              $schedule = json_decode($schedule);       
              
              $league_list = file_get_contents("http://www.sakkarinsai8.com/api-league/$my_customer_id");        
              $league_list = json_decode($league_list);
              
              $c_time = strtotime('now');
              $now_back_2_hour = $c_time + 7200;    
              
              $i = 0;
  
          ?>
          <?php foreach ($date as $key) { 
              
              if ($key == $yesterday) {
                  $t = 'ตารางถ่ายเมื่อวานนี้';
                  $t2 = 'วันนี้';
              }
  
              if ($key == $today) {
                  $t = 'รายการถ่ายทอดวันนี้';
                  $t2 = 'วันนี้';
              }
              else if($key == $tomorrow){
                  $t = 'รายการถ่ายทอดวันพรุ่งนี้';
                  $t2 = 'วันพรุ่งนี้';
              }
              $dd = DateThai($key);
              
              $sah_date = $key;
              $arrX = array_filter($schedule,
                  function ($element)
                  {
                      global $sah_date;                                          
                      if ($element->schedule_date == $sah_date) {
                          return $element;
                  }
              });
              $arrX = (array)$arrX;
  
          ?>
          
          <?php if(count($arrX) > 0){ ?>
  
         <table class="table" style="margin-top: 15px;">
          <tbody>
          	<caption>
              
                <div class="league">ตารางถ่ายทอดสดวันที่ <?php echo $dd; ?></div></caption>           
             

          	<tr>
              <th style="width: 8%;">เวลา</th>
              <th style="width: 28%; text-align: right;">เจ้าบ้าน</th>
              <th style="width: 8%;"></th>
              <th style="width: 28%; text-align: left;">ทีมเยือน</th>
              <th style="width: 19%;">ถ่ายทอดสด</th>
            </tr>         
           
            <?php foreach ($arrX as $key2 => $sch) { ?>
            <tr>
                <td style="vertical-align: middle;"><?php echo $sch->schedule_time; ?></td>
                <td style="text-align: right;vertical-align: middle;"><?php echo $sch->schedule_home; ?></td>
                <td style="vertical-align: middle;"><div><img onerror= this.src='assets/images/team_logo/noimage.jpg' width="35" src="assets/images/team_logo/<?php echo $sch->league_logo; ?>" ></div></td>
                <td style="text-align: left;vertical-align: middle;"><?php echo $sch->schedule_away; ?></td>
                <td style="vertical-align: middle;">
                    <!-- <a href="stream-test.php?ID=<?php echo $sch->schedule_channel[0]->channel_id; ?>&sch=<?php echo $sch->schedule_id; ?>"><img width="60px" src="img/link.gif"></a> -->
                    <div style="padding:3px;">
                        <?php 
                            $useragent=$_SERVER['HTTP_USER_AGENT'];

                            if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
                            {
                                $i = 2; 
                            }
                            else{
                                $i = 0; 
                            }                
                       
                        
                        ?>
                        <?php foreach($sch->schedule_channel as $cn) { $img_path = "/assets/images/channel_mini_logo/$cn->channel_img"; ?>
                            <?php if($i < 3): ?>
                            <a class="tooltips" href="stream-test.php?ID=<?php echo $cn->channel_id; ?>&sch=<?php echo $sch->schedule_id; ?>">                                
                                <img style="width:40px;" title="<?php echo "$cn->channel_name"; ?>" src="<?php echo $img_path; ?>">
                            </a>
                            <?php endif; ?>
                            
                        <?php $i++;  } ?>
                        </div>
                    </div>
                </td>
            </tr>
            <?php } ?>
    
        </tbody>
        </table>

        <?php } ?>
        <?php } ?>                    
        
      </div>
    </div>
  </div>
</div>

</section>

    <!-- jQuery -->
    <script src="lib/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/new-age.min.js"></script>

<script type="text/javascript">
window.onload = function(){
    document.getElementById('close').onclick = function(){
        this.parentNode.parentNode.parentNode
        .removeChild(this.parentNode.parentNode);
        return false;
    };
};
      $(document).ready(function(){
        $("#myModal").modal('show');
      });
    </script>
 <!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3962731,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?3962731&101" alt="" border="0"></a></noscript>
<!-- Histats.com  END  -->
</body>

</html>
