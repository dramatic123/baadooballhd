<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminBaadooball</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>Baadooball</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
              <!-- Menu Footer-->
              
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
          <!-- Control Sidebar Toggle Button -->
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class=" treeview">
          <li class="active treeview">
          <a href="program.php">
              <i class="fa fa-newspaper-o"></i> <span>ตารางถ่ายทอดสด</span>
          </a>
        </li>
          <li class="treeview">
          <a href="stream.php">
              <i class="fa fa-film"></i> <span>ช่องสตรีม</span>
          </a>
        </li>
        <li class="treeview">
          <a href="league.php">
              <i class="fa fa-film"></i> <span>ลีค</span>
          </a>
        </li>
        <li class="treeview">
          <a href="channel.php">
              <i class="fa fa-film"></i> <span>ช่อถ่ายทอดสด</span>
          </a>
        </li>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Program
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Program</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          
          <div class="box box-primary">
                <div class="row">
                  <div class="col-md-4">
                    <a href="program_more.php" class="btn btn-success btn-lg">เพิ่มโปรแกรมการแข่งขัน</a><<<<<< เพิ่มโปรแกรมการแข่งขันตรงนี้ !!!!!!
                  </div>
<?php include "config.php";
      $query = "SELECT * 
                 FROM `program` 
                 INNER JOIN `league` ON `program`.`league_id` = `league`.`league_id`
                 INNER JOIN `stream` ON `program`.`stream_id` = `stream`.`stream_id`
                 LEFT JOIN `channel` ON `program`.`channel_id` = `channel`.`channel_id` 
                 WHERE DATE_ADD(NOW(), INTERVAL 11 HOUR) <= `program`.`program_datetime`
                 ORDER by `program_datetime`";
      $result_select = $db->query($query) or die(mysql_error());
      $rows = array();
      while($row = mysqli_fetch_array($result_select))
          $rows[] = $row;
        $i=0;
?>
        <div class="col-md-12">
            <table class="table">
                        <thead>
                          <tr>
                            
                            <th>วันที่ - เวลา</th>
                            <th>ลีค</th>
                            <th>เจ้าบ้าน</th>
                            <th>ทีมเยือน</th>
                            <th>ช่องต้นทาง</th>
                            <th>ช่องถ่ายทอดสด</th>
                            <th>WARP</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                    <?php
                    foreach ($rows as $row){ 
                    $i++;
                    ?>
                          <tr>
                            
                            <td><?php echo substr($row['program_datetime'],0,16); ?></td>
                            <td><?php echo $row['league_name']; ?></td>
                             <td><?php echo $row['program_home']; ?></td>
                              <td><?php echo $row['program_away']; ?></td>
                               <td><?php echo $row['channel_name']; ?></td>
                                <td><?php echo $row['stream_name']; ?></td>
                                <td>http://baadooballhd.com/stream.php?ID=<?php echo $row['program_id']; ?></td>
                            <td>
                            <a href="#myModal<?php echo $row['program_id']; ?>" class="btn btn-primary" data-toggle="modal">edit</a>
                            <a href="program_delete.php?delete=<?php echo $row['program_id']; ?>" class="btn btn-danger" onclick="return  confirm('ยืนยันการลบ <?php echo $row['program_home']; ?> vs <?php echo $row['program_away']; ?>')">Delete</a></td>
                          </tr>
                    

                          <!-- Modal Edit -->
                          <div id="myModal<?php echo $row['program_id']; ?>" class="modal fade">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      </div>
                                      <form role="form" action="program_update.php" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="row">
                                          <div class="form-group col-md-6">
                            <input type="datetime-local" class="form-control" id="date" name="date" value="" placeholder="" required="กรุณาเลือก">
                          </div>
                <?php
                      include "config.php";
                      
                      $query = "SELECT * FROM league ORDER BY league_id ASC";
                      $result_select = $db->query($query) or die(mysql_error());
                      $leagues = array();
                      while($league = mysqli_fetch_array($result_select))
                          $leagues[] = $league;
                        $i=0;

                  ?>
                          <div class="form-group col-md-6">
                            <select class="form-control" name="league" required="กรุณาเลือก">
                              <option value="<?php echo $row['league_id']; ?>"  disabled hidden>ลีค</option>
                        <?php
                          foreach ($leagues as $league){ 
                            $i++;
                        ?>
                              <option value="<?php echo $league['league_id']; ?>" <?php if ($league['league_id']==$row['league_id']) {echo "selected";} ?>>
                                <?php echo $league['league_name']; ?></option>
                        <?php } ?>
                            </select>
                          </div>
                          <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="team1" name="home" value="<?php echo $row['program_home']; ?>" placeholder="เจ้าบ้าน" required="กรุณาใส่">
                          </div>
                          <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="team2" name="away" value="<?php echo $row['program_away']; ?>" placeholder="ทีมเยือน"  required="กรุณาใส่">
                          </div>
                  <?php
                      include "config.php";
                      
                      $query = "SELECT * FROM channel ORDER BY channel_id ASC";
                      $result_select = $db->query($query) or die(mysql_error());
                      $channels = array();
                      while($channel = mysqli_fetch_array($result_select))
                          $channels[] = $channel;
                        $j=0;

                  ?>
                          <div class="form-group col-md-6">
                            <select class="form-control" name="channel">
                              <option value="" selected disabled hidden>ช่องต้นทาง</option>
                        <?php
                          foreach ($channels as $channel){ 
                            $j++;
                        ?>
                             <option value="<?php echo $channel['channel_id']; ?>" <?php if ($channel['channel_id']==$row['channel_id']) {echo "selected";} ?>>
                              <?php echo $channel['channel_name']; ?></option>
                        <?php } ?>
                            </select>
                          </div>




                    <?php
                      include "config.php";
                      
                      $query = "SELECT * FROM stream ORDER BY stream_id ASC";
                      $result_select = $db->query($query) or die(mysql_error());
                      $streams = array();
                      while($stream = mysqli_fetch_array($result_select))
                          $streams[] = $stream;
                        $x=0;

                  ?>
                          <div class="form-group col-md-6">
                            <select class="form-control" name="stream" required="กรุณาเลือก">
                              <option value="" selected disabled hidden>ช่องที่ถ่าย</option>
                        <?php
                          foreach ($streams as $stream){ 
                            $x++;
                        ?>
                             <option value="<?php echo $stream['stream_id']; ?>" <?php if ($stream['stream_id']==$row['stream_id']) {echo "selected";} ?>>
                              <?php echo $stream['stream_name']; ?></option>
                        <?php } ?>
                            </select>
                          </div>
                                        </div>
                                          <input type="hidden" name="id" value="<?php echo $row['program_id']; ?>">
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          <button type="submit" class="btn btn-primary">Save changes</button>
                                      </div>
                                      </form>

                                  </div>
                              </div>
                          </div>
                          <?php } ?>

                        </tbody>
                      </table>
                       </div>
           </div>
          </div>
        </div>
      </div>
    </section>
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/timepicker/bootstrap-timepicker.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>


</body>
</html>
