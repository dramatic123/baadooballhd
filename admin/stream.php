<?php
  session_start();
  if($_SESSION['admin_id'] == "")
  {
    echo "Please Login!";
    exit();
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminBaadooball</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/toggle.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap2-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap2-toggle.min.js"></script>

<link rel="stylesheet" href="plugins/image-picker/image-picker.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>Baadooball</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
              <!-- Menu Footer-->
              
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
          <!-- Control Sidebar Toggle Button -->
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class=" treeview">
          <li class="treeview">
          <a href="program.php">
              <i class="fa fa-newspaper-o"></i> <span>ตารางถ่ายทอดสด</span>
          </a>
        </li>
          <li class="active treeview">
          <a href="stream.php">
              <i class="fa fa-film"></i> <span>ช่องสตรีม</span>
          </a>
        </li>
        <li class="treeview">
          <a href="league.php">
              <i class="fa fa-film"></i> <span>ลีค</span>
          </a>
        </li>
        <li class="treeview">
          <a href="channel.php">
              <i class="fa fa-film"></i> <span>ช่อถ่ายทอดสด</span>
          </a>
        </li>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Stream
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Stream</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     <div class="row">
<?php include "config.php";
      $query = "SELECT * FROM stream ORDER BY stream_name ASC";
      $result_select = $db->query($query) or die(mysql_error());
      $rows = array();
      while($row = mysqli_fetch_array($result_select))
          $rows[] = $row;
        $i=0;
?>
        <div class="col-md-12">
            <table class="table">
                        <thead>
                          <tr>
                            <th>NAME</th>
                            <th>ช่องหลัก</th>
                            <th>สถานะช่องสำรอง</th>
                            <th>สำรอง 1</th>
                            <th>สำรอง 2</th>
                            <th><a href="#addModal" class="btn btn-success" data-toggle="modal">Add Channel</a></th>
                          </tr>
                        </thead>
                        <tbody>
                    <?php
                    foreach ($rows as $row){ 
                    $i++;
                    ?>
                          <tr>
                            <td><?php echo $row['stream_name']; ?></td>
                            <td><?php echo $row['stream_path']; ?></td>
                            <td><label>
                                <input type="checkbox" class="btn_changepermission" data-id="<?php echo $row['stream_id']; ?>" data-toggle="toggle" data-onstyle="success" <?php if ($row['stream_status']=='1') {echo "checked";} ?>>
                              </label></td>
                            <td><?php echo $row['stream_sub1']; ?></td>
                            <td><?php echo $row['stream_sub2']; ?></td>
                            <td>
                            <a href="#myModal<?php echo $row['stream_id']; ?>" class="btn btn-primary" data-toggle="modal">edit</a>
                            <a href="stream_delete.php?delete=<?php echo $row['stream_id']; ?>" class="btn btn-danger" onclick="return  confirm('ยืนยันการลบ <?php echo $row['stream_name']; ?>')">Delete</a></td>
                          </tr>
                    

                          <!-- Modal Edit -->
                          <div id="myModal<?php echo $row['stream_id']; ?>" class="modal fade">
                               <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      </div>
                                      <form role="form" action="stream_update.php" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="row">
                                          <div class="col-md-3">
                                            Name
                                          </div>
                                          <div class="col-md-9">
                                           <input type="text" class="form-group" name="name" value="<?php echo $row['stream_name']; ?>">
                                          </div>
                                          <div class="col-md-3">
                                            URL
                                          </div>
                                          <div class="col-md-9">
                                           <input type="file" class="form-group" name="url" value="<?php echo $row['stream_path']; ?>">
                                          </div>
                                          <div class="col-md-3">
                                            ช่องสำรอง 1
                                          </div>
                                          <div class="col-md-9">
                                           <input type="file" class="form-group" name="sub1" value="<?php echo $row['stream_sub1']; ?>">
                                          </div>
                                          <div class="col-md-3">
                                            ช่องสำรอง 2
                                          </div>
                                          <div class="col-md-9">
                                           <input type="file" class="form-group" name="sub2" value="<?php echo $row['stream_sub2']; ?>">
                                          </div>
                                        </div>
                                          <input type="hidden" name="id" value="<?php echo $row['stream_id']; ?>">
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          <button type="submit" class="btn btn-primary">Save changes</button>
                                      </div>
                                      </form>

                                  </div>
                              </div>
                          </div>
                          <?php } ?>


                          <!-- Modal Add -->
                          <div id="addModal" class="modal fade">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      </div>
                                      <form role="form" action="stream_add.php" method="post" enctype="multipart/form-data">
                                      <div class="modal-body">
                                        <div class="row">
                                          <div class="col-md-3">
                                            Name
                                          </div>
                                          <div class="col-md-9">
                                           <input type="text" class="form-group" name="name" value="">
                                          </div>
                                          <div class="col-md-3">
                                            File
                                          </div>
                                          <div class="col-md-9">
                                           <input type="file" class="form-group" name="url" value="">
                                          </div>
                                        </div>

                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          <button type="submit" class="btn btn-primary">Save changes</button>
                                      </div>
                                      </form>

                                  </div>
                              </div>
                          </div>
                        </tbody>
                      </table>
           </div>

      </div>
    </section>
</div>

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->


<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);

  $(document).ready(function() {
   // $('.btn_changepermission').bootstrapSwitch({size : 'small'});
    var stopchange = false;
    
    $('.btn_changepermission').change(function(){
      var obj = $(this);
        $.ajax({
                url: "/admin/stream_status_update.php",
                dataType: 'json',
                type: "POST",
                quietMillis: 100,
                data: { 
                    stream_id: $(this).data('id'),
                    stream_status: $(this).prop('checked')
                },
        });





    });
        
        
        
  }); 
</script>
</body>
</html>